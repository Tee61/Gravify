# Gravify
Spotifyfake von Nick und Niko
Spotify - 
Mit Hilfe von WPF und c# haben wir einen "Spotifyfake" programmiert. Mit "Gravify" kann man mit dem Button "Songs hinzufügen", mp3 Files zur Bibliothek hinzufügen. Wichtig dabei ist dass die Metadaten ausgelesen werden und der Song titel den selben namen wie die Datei haben muss. Im Songfolder sind schon vorgefertigte Songs hinterlegt.

Beim Starten des Programms müssen noch 3 Variablen geändert werden.
1. dataPath beschreibt den Ort wo alle jSON Files abgespeichert und aufgerufen werden
2. libraryPath beschreibt den Ort wo alle Songs liegen
3. im CreatePlaylist.cs muss der Path zum gleichen Pfad geändert werden wie datapath nur mit "\playlists.json" hinten hinzugefügt.

Nach dem Starten und hinzufügen von Songs können diese in der Bibliothek oder vorher erstellten Playlists angehört werden. Mit den Buttons links und rechts vom Play button oben rechts können Lieder übersprungen oder nocheinmal angehört werden.

V1:
 Lieder in Bibliothek hinzufügen, check
			Lieder aus Explorer auswählen (mp3 Files)
			Metadaten werden ausgelesen und Songs in objekte gespeichert
			In Liste geladen und als Json gespeichert

 Bibliothek laden (JSON), check
			Durch Knopfdruck wird die Liste der Songs Dargestellt mit
			Metadaten usw

 Playlists erstellen,
			Playlist objekt wird erstellt bzw wird zu bestehender Liste hinzugefügt

 Playlists laden,
			Die Liste wird mit Buttons dargestellt welche gedrückt werden können
			um die Songs in der Playlist anzuschauen

 Lieder Shufflen,
			in zufälliger reihenfolge werden songs "abgespielt" also angezeigt

 Einzelne Lieder abspielen (Nur mit Membership),
			durch anklicken der Songs wird der song als abspielend angezeigt

V2:
 Membership
			Mit membership können einzelne lieder abgespielt werden
 Lieder wirklich abspielen
			Pfad des angeklickten liedes wird gesucht
			mp3 wird abgespielt

 Covers von Liedern
			Albumcover wenn das lied abgespielt wird
			(und in der Bibliothek)

 Letztes Lied merken (mit Timestamp)
			wenn das Programm geschlossen wird wird in einer Json File
			vermerkt welches lied man zuletzt abgespielt hat und bei
			welcher Minute man war

 Biliothek sortieren nach Künstler, Alphabet, ...
			mit Button sortieren

 Künstler Profil
			Alle lieder eines Künstlers mit Playlists und Alben

